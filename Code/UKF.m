function [xcap_k_k,Pxxcap_k_k] = UKF(xcap_km1_km1, Pxxcap_km1_km1, Q_km1, R_k, measurement_Yk, f_state_eqn, SPsPar,UNITS,materialInput,ParameterInfo,NstepG,GMinput,StepUpdateSize,numResp,Constraints,NormalizeWith)

    % generate sigma points(SPs) for x(k-1|k-1) pdf
    [x_km1_km1_SPs,StepSize] = GenerateSPs(xcap_km1_km1,Pxxcap_km1_km1,SPsPar,Constraints); %xcap_km1_km1 = R(ntheta)

    % propagate SPs of x(k-1|k-1) through state equation
    x_k_km1_SPs = f_state_eqn(x_km1_km1_SPs); %x_k_km1_SPs = (ntheta x 2ntheta+1)

    %%% Parameter Prediction
    % calculate statistics of x(k|k-1)
    [xcap_k_km1,Pxxcap_k_km1,~,~] = UT(x_k_km1_SPs,SPsPar,Q_km1,StepSize,Constraints); %xcap_k_km1 = (ntheta x 1)

    % can again call GenerateSPs to calculate SPs of x(k|k-1) based on above calculated mean and covariance %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %[x_k_km1_SPs] = GenerateSPs(xcap_k_km1,Pxxcap_k_km1,SPsPar);

    % propagate SPs of though measument equation
    y_k_km1_SPs = h_measurement_eqn(x_k_km1_SPs,UNITS,materialInput,ParameterInfo,NstepG,GMinput,StepUpdateSize,numResp,NormalizeWith); %y_k_km1_SPs = (Dny x 2ntheta+1)

    %%% Observation Prediction
    % calculate statistics of y(k|k-1)
    [ycap_k_km1,Pyycap_k_km1,~,Wc] = UT(y_k_km1_SPs,SPsPar,R_k,StepSize,Constraints); %ycap_k_km1 = (Dny x 1) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% R_k

    % cross-covariance P(cap)(x_y)(k|k-1)
    Pxycap_k_km1 = (x_k_km1_SPs-xcap_k_km1)*diag(Wc)*(y_k_km1_SPs-ycap_k_km1)'; %(ntheta x Dny)

    % Kalman Gain
    Kk = Pxycap_k_km1/Pyycap_k_km1; %(ntheta x Dny)
    
    if strcmp(Constraints.constrainFlag,'no') == 1
        % Update Parameter Estimate
        xcap_k_k = xcap_k_km1 + Kk*(measurement_Yk - ycap_k_km1); %(ntheta x 1)
        % Update Parameter Covariance
        Pxxcap_k_k = Pxxcap_k_km1 - Kk*Pyycap_k_km1*Kk'; %(ntheta x ntheta)
        
    elseif strcmp(Constraints.constrainFlag,'yes') == 1
        
        % Update each SP with kalman updating equation
        x_k_k_SPs = x_k_km1_SPs + Kk*(measurement_Yk - y_k_km1_SPs); %(ntheta x 1)
        
        % Update Parameter Estimate and Covariance
        [xcap_k_k,Pxxcap_k_k,~,~] = UT(x_k_k_SPs,SPsPar,Q_km1+Kk*R_k*Kk',StepSize,Constraints);
        
        % if the estimate doesn't satisfy the constraints
        if (sum((xcap_k_k>=Constraints.Lower).*(xcap_k_k<=Constraints.Upper)) ~= size(xcap_k_k,1))
            % project the SPs onto the constrained boundary
            x_k_k_SPs_C = max(min(x_k_k_SPs,Constraints.Upper),Constraints.Lower);
            % reestimate the parameter and its covariance
            %[xcap_k_k,Pxxcap_k_k,~,~] = UT(x_k_k_SPs_C,SPsPar,Q_km1+Kk*R_k*Kk',StepSize,Constraints); % both estimate and covariance are updated according to projectected SPs
            [xcap_k_k,~,~,~] = UT(x_k_k_SPs_C,SPsPar,Q_km1+Kk*R_k*Kk',StepSize,Constraints); % only estimate is updated according to projectected SPs
        end
    end
    
    % Nonlinear function s = g(z) % y = g(theta)
    function [Zsp,StepSize] = GenerateSPs(muZ,Pz,SPsPar,Constraints) %muz = (ntheta x 1) %%muz = (ntheta x ntheta)   
        % scaled UT defines the following sigma points
        % matrix Pz square root is computed using numerically efficient and stable methods: Cholesky decomposition.
        
        % maintain symmetry (for covariance matrix)
        Pz = (0.5*Pz) + (0.5*Pz)';
        % cholesky decomposition
        [cholPz,ok] = chol(Pz);
        if ok ~=0 %Pz is not positive definite
            Pz = Pz + eps*eye(size(Pz,1));
            [cholPz,~] = chol(Pz);
        end
        
        ntheta = size(muZ,1);
        Term1 = repmat(muZ,[1 2*ntheta]);
        Term2 = [cholPz' -cholPz'];
        defStepSize = repmat(SPsPar.gamma,[1 2*ntheta]);
        
        if strcmp(Constraints.constrainFlag,'no') == 1
            Zsp = [muZ Term1+defStepSize.*Term2];
            StepSize = defStepSize;
        elseif strcmp(Constraints.constrainFlag,'yes') == 1
            L = repmat(Constraints.PreLower,[1 2*ntheta]);
            U = repmat(Constraints.PreUpper,[1 2*ntheta]);
            
            thetaL = min([defStepSize;abs((L-Term1)./min(Term2,eps))]);
            thetaU = min([defStepSize;abs((U-Term1)./max(Term2,eps))]);
            
            StepSizei = min([thetaL;thetaU]);
            
            StepSizeForSymmetry = min([StepSizei(1:ntheta);StepSizei(ntheta+1:2*ntheta)]);
            StepSizeForSymmetry = repmat(StepSizeForSymmetry,[1 2]);
            
            StepSize = StepSizei; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            Zsp = [muZ Term1+StepSize.*Term2];
        end
        
        %Zsp0 = muZ;
        %Zsp1 = muZ + (SPsPar.gamma*cholPz)';
        %Zsp2 = muZ - (SPsPar.gamma*cholPz)';
        %Zsp = [Zsp0 Zsp1 Zsp2]; %(ntheta x 2ntheta+1)
    end
    
    % Nonlinear function s = g(z) % y = g(theta)
    function [muS,Pss,Wm,Wc] = UT(gZsps,SPsPar,noiseCOVARIANCE,StepSize,Constraints) %gZsps = (ny x 2ntheta+1)
        
        if strcmp(Constraints.constrainFlag,'no') == 1
            if strcmp(SPsPar.UTflag,'UT') == 1
                % weight coefficients of SPs to estimate mean of s: muS
                Wm0 = SPsPar.kappa/(SPsPar.nz+SPsPar.kappa);
                Wmi = (1/(2*(SPsPar.nz+SPsPar.kappa)))*ones(1,2*SPsPar.nz);
                Wm = [Wm0 Wmi];
                
                % weight coefficients of SPs to estimate covariance of s: Pss
                Wc = Wm;
            elseif strcmp(SPsPar.UTflag,'SUT') == 1
                % weight coefficients of SPs to estimate mean of s: muS
                Wm0 = SPsPar.lambda/(SPsPar.nz+SPsPar.lambda);
                Wmi = (1/(2*(SPsPar.nz+SPsPar.lambda)))*ones(1,2*SPsPar.nz);
                Wm = [Wm0 Wmi];

                % weight coefficients of SPs to estimate covariance of s: Pss
                Wc0 = (SPsPar.lambda/(SPsPar.nz+SPsPar.lambda))+(1-SPsPar.alpha^2+SPsPar.beta);
                Wci = (1/(2*(SPsPar.nz+SPsPar.lambda)))*ones(1,2*SPsPar.nz);
                Wc = [Wc0 Wci];
            end
        elseif strcmp(Constraints.constrainFlag,'yes') == 1
            if strcmp(SPsPar.UTflag,'UT') == 1
                Stheta = sum(StepSize);
                a = (2*SPsPar.kappa-1)/(2*(SPsPar.nz+SPsPar.kappa)*(Stheta - (2*SPsPar.nz+1)*(sqrt(SPsPar.nz+SPsPar.kappa))));
                b = (1/(2*(SPsPar.nz+SPsPar.kappa)))-((2*SPsPar.kappa-1)/(2*(sqrt(SPsPar.nz+SPsPar.kappa))*(Stheta - (2*SPsPar.nz+1)*(sqrt(SPsPar.nz+SPsPar.kappa)))));

                % weight coefficients of SPs to estimate mean of s: muS
                Wmi = a*StepSize+b;
                Wm0 = 1-sum(Wmi);
                Wm = [Wm0 Wmi];

                % weight coefficients of SPs to estimate covariance of s: Pss
                Wc = Wm;
            end
        end
        
        
        % estimate of mean and covariance matrix of s
        muS = (Wm*gZsps')'; %(ny x 1)
        Pss = (gZsps-muS)*diag(Wc)*(gZsps-muS)' + noiseCOVARIANCE; %(ny x ny)
    end
    
    function [Normalizedresponse] =  h_measurement_eqn(parameters_SPs,UNITS,materialInput,ParameterInfo,NstepG,GMinput,StepUpdateSize,numResp,NormalizeWith)
        numSPs = size(parameters_SPs,2);
        for i1 = 1:numSPs
            statusFile = fopen(sprintf('status(SP%d).txt',i1),'w'); fprintf(statusFile,'%d',1); fclose(statusFile); %set status to not successful
            
            UKFparID = fopen(sprintf('UKFpar%d.tcl',i1),'w');
            fprintf(UKFparID,'%s\n','wipe;');
            fprintf(UKFparID,'%s%d%s\n','set SPnum SP',i1,';');
            fprintf(UKFparID,'%s%s%s\n','source UNITS',UNITS,'.tcl;');
            fprintf(UKFparID,'%s%s%s\n','set matType ',materialInput.matType,';');
            fprintf(UKFparID,'%s%e%s\n','set rhoM ',materialInput.matDensity,';');
            countU = 1;
            countNU = 1;
            for j = 1:length(ParameterInfo.ParameterName)
                if ParameterInfo.UpdateParameterIndex(j) == 1
                    fprintf(UKFparID,'%s%s%s%f%s\n','set ',ParameterInfo.ParameterName{j},' ',parameters_SPs(countU,i1)*ParameterInfo.TrueUpdateParameterValues(countU),';');
                    countU = countU + 1;
                else
                    fprintf(UKFparID,'%s%s%s%f%s\n','set ',ParameterInfo.ParameterName{j},' ',ParameterInfo.NotUpdatingParametersValues(countNU),';');
                    countNU = countNU + 1;
                end
            end
            fprintf(UKFparID,'%s%e%s\n','set tol ',materialInput.matTolerance,';');
            fprintf(UKFparID,'%s%d%s\n','set NstepG ',NstepG,';');
            fprintf(UKFparID,'%s\n','set GMFile "NoisyGM.txt";');
            %fprintf(UKFparID,'%s%f%s\n','set GMfactor [expr ',GMinput.GMfactor,'*$g];');
            %fprintf(UKFparID,'%s%d%s\n','set GMdirection ',GMinput.GMdirection,';');
            fprintf(UKFparID,'%s%f%s\n','set DtAnalysis ',GMinput.DtAnalysis,';');
            fprintf(UKFparID,'%s%d%s\n','set Nsteps ',GMinput.Nsteps,';');
            %fprintf(UKFparID,'%s%d%s\n','set DampModei ',DampingInput.DampModei,';');
            %fprintf(UKFparID,'%s%d%s\n','set DampModej ',DampingInput.DampModej,';');
            %fprintf(UKFparID,'%s%f%s\n','set ksi ',DampingInput.ksi,';');
            fprintf(UKFparID,'%s\n','set dataDir "UKF Results";');
            fprintf(UKFparID,'%s\n','source runUKF.tcl;');
            fprintf(UKFparID,'%s','wipe;');
            fclose(UKFparID);
        end
        
        runcounter = ones(numSPs,1);
        status = ones(numSPs,1);
        
        % run opensees (can be run in parallel)
        parfor i2 = 1:numSPs
            [runcounter(i2),~] = system(['OpenSees_Cap UKFpar',num2str(i2),'.tcl']);
        end
        
        % check for run success (bullet proof)
        i4 = 1;
        while i4 <= numSPs
            status(i4) = load(sprintf('status(SP%d).txt',i4));
            if (status(i4) == 0 && runcounter(i4) ==0) %run was successful
                i4 = i4+1;
                materialInput.matTolerance = 1.0e-6;
            elseif (status(i4) ~= 0 || runcounter(i4) ~=0) %run is unsuccessful
                if materialInput.matTolerance == 1.0e-6
                    materialInput.matTolerance = 1.0e-7;
                    [runcounter(i4)] = runagain(i4,materialInput.matTolerance);
                elseif materialInput.matTolerance == 1.0e-7
                    materialInput.matTolerance = 1.0e-5;
                    [runcounter(i4)] = runagain(i4,materialInput.matTolerance);
                elseif materialInput.matTolerance == 1.0e-5
                    materialInput.matTolerance = 1.0e-8;
                    [runcounter(i4)] = runagain(i4,materialInput.matTolerance);
                elseif materialInput.matTolerance == 1.0e-8
                    materialInput.matTolerance = 1.0e-4;
                    [runcounter(i4)] = runagain(i4,materialInput.matTolerance);
                elseif materialInput.matTolerance == 1.0e-4
                    i4 = i4+1;
                end
            end
        end

        Normalizedresponse = zeros(StepUpdateSize*sum(numResp),numSPs);
        cd('UKF Results');
        for i3 = 1:numSPs
            respSP = zeros(GMinput.Nsteps,sum(numResp));
            % Accelerometers
            for k1 = 1:numResp(1)
                temp = load(sprintf('(SP%d)Acc%d.txt',i3,k1));
                delete(sprintf('(SP%d)Acc%d.txt',i3,k1));
                respSP(:,k1) = temp(:,2)+GMinput.NoisyGM(1:GMinput.Nsteps);
                clearvars temp;
            end
            % Displacement Sensors
            for k2 = 1:numResp(2)
                temp = load(sprintf('(SP%d)Disp%d.txt',i3,k2));
                delete(sprintf('(SP%d)Disp%d.txt',i3,k2));
                respSP(:,numResp(1)+k2) = temp(:,2);
                clearvars temp;
            end
            NormalizedrespSP = respSP./NormalizeWith;
            Normalizedresponse(:,i3) =  reshape(NormalizedrespSP(end-StepUpdateSize+1:end,:)',[StepUpdateSize*sum(numResp),1]);
        end
        cd('..');
        
        
        function [rc] = runagain(SP,tol)
            ChangeFileData = regexp( fileread(sprintf('UKFpar%d.tcl',SP)), '\n', 'split');
            ChangeFileData{17} = strcat("set tol ",num2str(tol,'%e'),";"); %17 is tolerance line
            fid = fopen(sprintf('UKFpar%d.tcl',SP), 'w');
            fprintf(fid, '%s\n', ChangeFileData{:});
            fclose(fid);
            [rc,~] = system(['OpenSees_Cap UKFpar',num2str(SP),'.tcl']);
        end
        
    end

end



%     % Nonlinear function s = g(z) % y = g(theta)
%     function [Zsp] = GenerateSPs(muZ,Pz,SPsPar) %muz = (ntheta x 1) %%muz = (ntheta x ntheta)   
%         % scaled UT defines the following sigma points
%         % matrix Pz square root is computed using numerically efficient and stable methods: Cholesky decomposition.
%         
%         % maintain symmetry (for covariance matrix)
%         Pz = (0.5*Pz) + (0.5*Pz)';
%         % cholesky decomposition
%         [cholPz,ok] = chol(Pz);
%         if ok ~=0 %Pz is not positive definite
%             Pz = Pz + eps*eye(size(Pz,1));
%             [cholPz,~] = chol(Pz);
%         end
% 
%         Zsp0 = muZ;
%         Zsp1 = muZ + (SPsPar.gamma*cholPz)';
%         Zsp2 = muZ - (SPsPar.gamma*cholPz)';
%         Zsp = [Zsp0 Zsp1 Zsp2]; %(ntheta x 2ntheta+1)
%     end


%     % Nonlinear function s = g(z) % y = g(theta)
%     function [muS,Pss] = UT(gZsps,SPsPar,noiseCOVARIANCE) %gZsps = (ny x 2ntheta+1)
%         % estimate of mean and covariance matrix of s
%         muS = (SPsPar.Wm*gZsps')'; %(ny x 1)
%         Pss = (gZsps-muS)*diag(SPsPar.Wc)*(gZsps-muS)' + noiseCOVARIANCE; %(ny x ny)
%     end