function [I1f1,NormSf1,I1f2,NormSf2,I1f3,NormSf3] = CapDataToPlot(ParameterInfo,ParamterValues)

    count = 1;
    for j = 1:length(ParameterInfo.ParameterName)
        if ParameterInfo.UpdateParameterIndex(j) == 1
            CapPar.(ParameterInfo.ParameterName{j}) = ParamterValues(count);
            count = count + 1;
        else
            CapPar.(ParameterInfo.ParameterName{j}) = ParameterInfo.TrueParameterValues(j);
        end
    end
    
    % hardening parameter k;
    Fe = @(x) CapPar.alpha-CapPar.lambda*exp(-CapPar.beta*x)+CapPar.theta*x;
    Xk = @(k) k+CapPar.R*Fe(k);
    options = optimoptions(@fsolve,'Display','off');
    k = fsolve(@(k) Xk(k)-CapPar.X,1000,options);
    
    if k > 0
        Lk = k;
    elseif k <= 0
        Lk = 0;
    end
    I1f1 = CapPar.T:(k-CapPar.T)/1000:k;
    NormSf1 = Fe(I1f1);
    % f2
    I1f2 = k:(CapPar.X-k)/1000:CapPar.X;
    NormSf2 = (Fe(k)^2 - ((I1f2-Lk)/CapPar.R).^2).^(0.5);
    % f3
    I1f3 = [CapPar.T;CapPar.T];
    NormSf3 = [0;Fe(CapPar.T)];
    
end