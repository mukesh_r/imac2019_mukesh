#set dataDir "Results";					# set up name of data directory
file mkdir $dataDir; 					# create data directory

set AccNodeRec {91 63 49 7};
set DispNodeRec {91 63 49 7};

set count 0;
for {set i 1} {$i<=[llength $AccNodeRec]} {incr i} {
	set count [expr $count+1];
	recorder Node -file $dataDir/($SPnum)Disp$count.txt -time -node [lindex $DispNodeRec [expr $i-1]] -dof 1 disp;
	recorder Node -file $dataDir/($SPnum)Acc$count.txt -time -node [lindex $AccNodeRec [expr $i-1]] -dof 1 accel;
}

set StressEleRec1 {49 37 1};
set StressEleRec2 {54 42 6};

set count 0;
for {set i 1} {$i<=[llength $StressEleRec1]} {incr i} {
	set count [expr $count+1];
	recorder Element -ele [lindex $StressEleRec1 [expr $i-1]] -time -file $dataDir/($SPnum)stress$count.txt -time   material 1 stress;
	recorder Element -ele [lindex $StressEleRec1 [expr $i-1]] -time -file $dataDir/($SPnum)strain$count.txt -time   material 1 strain;
	recorder Element -ele [lindex $StressEleRec1 [expr $i-1]] -time -file $dataDir/($SPnum)k$count.txt -time   material 1 k;
	
	set count [expr $count+1];
	recorder Element -ele [lindex $StressEleRec2 [expr $i-1]] -time -file $dataDir/($SPnum)stress$count.txt -time   material 2 stress;
	recorder Element -ele [lindex $StressEleRec2 [expr $i-1]] -time -file $dataDir/($SPnum)strain$count.txt -time   material 2 strain;
	recorder Element -ele [lindex $StressEleRec2 [expr $i-1]] -time -file $dataDir/($SPnum)k$count.txt -time   material 2 k;
}