function GM = ModifyGM(GMinput,g)

    GMwPwT = load('GroundMotionFile.txt');
    recordedN = GMwPwT(1);
    recordedDT = GMwPwT(2);
    
    resampledGM = resample(GMwPwT(3:end),1/GMinput.DtAnalysis,1/recordedDT);
    
    GM = (GMinput.GMfactor*g)*resampledGM(1:GMinput.Nsteps);
    
    fileID = fopen('GM.txt','w');
    fprintf(fileID,'%.15f\n',GM);
    fclose(fileID);

end

