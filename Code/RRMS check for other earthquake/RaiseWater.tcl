#set rhoWater [expr 62.3387*$pcf];
set rhoWater [expr (1000.0*$thick*9.81*$N/pow($m,3))/$g]; #(kip/(in/s2))/in3

set NumNodesPerLayer [expr $NumXBlocks+1];

# if {$Display == "yes"} {
# recorder display "Displaced shape" 30 30 600 600 -wipe;
# prp 0 20 1;
# vup 0 1 0;
# vpn 0 0 1;
# display 1 1 50;
# set Display no;
# }

integrator LoadControl 0.1;
system BandGeneral;
test NormDispIncr 1.0e-8 100;
numberer Plain;
constraints Plain;
algorithm Newton;
analysis Static;


set PrevLoad {0.0};
#set PrevLoad {0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0};

for {set layer 1} {$layer <= [expr $NumYBlocksBot+$NumYBlocksTop]} {incr layer} {
	
	set patternTag $layer;
	pattern Plain $patternTag "Linear" {
		
		for {set ilayer $layer} {$ilayer > 0} {incr ilayer -1} {
			set HCele [expr [lindex $Ycord $ilayer]-[lindex $Ycord [expr $ilayer-1]]];
			
			if {$ilayer == $layer} {
				set Thei [expr 0.5*$HCele];
				load [expr ($ilayer*$NumNodesPerLayer)+1] [expr $rhoWater*$g*(0.5*$Thei*$Thei)] 0.0;
				#puts "load [expr ($ilayer*$NumNodesPerLayer)+1] [expr $rhoWater*$g*(0.5*$Thei*$Thei)] 0.0;"
				lappend PrevLoad [expr $rhoWater*$g*(0.5*$Thei*$Thei)];
				set Area [expr 0.5*$Thei*$Thei];
				set HPele $HCele;
			} else {
				set Thei [expr $Thei+0.5*($HCele+$HPele)];
				load [expr ($ilayer*$NumNodesPerLayer)+1] [expr ($rhoWater*$g*((0.5*$Thei*$Thei)-$Area))-([lindex $PrevLoad $ilayer])] 0.0;
				#puts "load [expr ($ilayer*$NumNodesPerLayer)+1] [expr ($rhoWater*$g*((0.5*$Thei*$Thei)-$Area))-([lindex $PrevLoad $ilayer])] 0.0;"
				set PrevLoad [lreplace $PrevLoad $ilayer $ilayer [expr $rhoWater*$g*((0.5*$Thei*$Thei)-$Area)]];
				set Area [expr 0.5*$Thei*$Thei];
				set HPele $HCele;
			}
		}
		load 1 [expr ($rhoWater*$g*((0.5*[lindex $Ycord $layer]*[lindex $Ycord $layer])-$Area))-([lindex $PrevLoad 0])] 0.0;
		#puts "load 1 [expr ($rhoWater*$g*((0.5*[lindex $Ycord $layer]*[lindex $Ycord $layer])-$Area))-([lindex $PrevLoad 0])] 0.0;"
		set PrevLoad [lreplace $PrevLoad 0 0 [expr $rhoWater*$g*((0.5*[lindex $Ycord $layer]*[lindex $Ycord $layer])-$Area)]];
		
		#puts "$PrevLoad"
	}
	#puts "------------------------------"
	
	set ok [analyze 10];
	
	# if {$ok != 0}  {
		# puts "NOT CONVERGED for Pattern $patternTag";
		# puts "============================";
	# } else {
		# puts "CONVERGED for Pattern $patternTag";
		# puts "=========================";
	# }
	loadConst -time 0.0;
}