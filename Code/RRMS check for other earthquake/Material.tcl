#=================================================
# DEFINE MATERIAL
#=================================================

if {$matType == "ElasticIsotropic"} {
	#ElasticIsotropic material object
	#nDMaterial ElasticIsotropic $matTag $E $v <$rho>
	nDMaterial ElasticIsotropic $matTag $E $v $rhoM;
} elseif {$matType == "CapPlasticity"} {
	#          CapPlasticity $tag 		$ndm 	$rho  	$G   	$K    	$X   	$D     	$W  	$R   	$lambda	$theta	$beta 	$alpha 	$T  	$tol
	nDMaterial CapPlasticity $matTag	$ndm 	$rhoM  	$G   	$K    	$X   	$D     	$W  	$R   	$lambda	$theta	$beta 	$alpha 	$T  	$tol;
	#nDMaterial CapPlasticity 1    2    $rhoM  11721092000.  14478996000.  1.10276e8 4.6412e-10 0.42 4.43 7.9979e6 0.11 6.3816e-8 2.6614e7 -2.0684e6 1.0e-8
} elseif {$matType == "TruncatedDP"} {
	#          TruncatedDP $tag 	$ndm 	$rho  	$G   	$K    	$theta		$alpha 		$T  	$tol
	nDMaterial TruncatedDP $matTag	$ndm 	$rhoM  	$G   	$K    	$thetaTDP	$alphaTDP 	$T  	$tol;
}