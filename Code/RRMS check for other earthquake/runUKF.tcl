#wipe;
#set ExportID [open "ModelData.txt" "w"];		###
#set ExportPA [open "CapParameters.txt" "w"];	###

#source UKFpar.txt;
#===============================================
# DEFINE UNITS
#===============================================
# Basic units
#source UNITSUS.tcl; #kip, in, sec
#source UNITSSI.tcl; #N, m, sec

#===============================================
# CREATE MODEL BUILDER (2D)
#===============================================
set ndm 2;	#spacial dimension of problem
set ndf 2;	#number of dof's per node

#model BasicBuilder -ndm $ndm <-ndf $ndf>
model BasicBuilder -ndm $ndm -ndf $ndf;

#===============================================
# DAM GEOMETRY AND MESH
#===============================================
set height 	[expr 400.*$ft];
set base	[expr 314.*$ft];
set top		[expr 32.*$ft];
set topHei	[expr 65.*$ft];

#set topbot	[expr 33.5*$ft];
set HandleIrregularity Curve; #ChangeEntireDam or JustChangeNode or Curve

set Zcord {0.0}; #in or m

# Define Mesh
set NumYBlocksBot	8;
set NumYBlocksTop	4;

set NumXBlocks		6;

#===============================================
# CREATE NODES and ADD BOUNDARY CONDITIONS
#===============================================
source XYcord2.tcl;
source NodesAndBCs.tcl;

#===============================================
# MATERIAL and its PARAMETERS
#===============================================
set matTag 	1;

# set matType CapPlasticity; #ElasticIsotropic, CapPlasticity

# #density
# set rhoM 	[expr (2400.*9.81*$N/pow($m,3))/$g]; #(kip/(in/s2))/in3

# #CapPlasticityParameters
# set G 		[expr 1700.0*$ksi]
# set K		[expr 2100.0*$ksi]
# set X		[expr 16.0*$ksi]
# set D 		[expr 0.0032*(1./$ksi)]
# set W		[expr 0.42]
# set R		[expr 4.43]
# set lambda 	[expr 1.16*$ksi]
# set theta 	[expr 0.11]
# set beta 	[expr 0.441*(1./$ksi)]
# set alpha 	[expr 3.86*$ksi]
# set T 		[expr -0.3*$ksi]

# puts $ExportPA "set G 		[expr 1700.0*$ksi]
# set K		[expr 2100.0*$ksi]
# set X		[expr 16.0*$ksi]
# set D 		[expr 0.0032*(1./$ksi)]
# set W		[expr 0.42]
# set R		[expr 4.43]
# set lambda 	[expr 1.16*$ksi]
# set theta 	[expr 0.11]
# set beta 	[expr 0.441*(1./$ksi)]
# set alpha 	[expr 3.86*$ksi]
# set T 		[expr -0.3*$ksi]"

# set tol 1.0e-8;

# #ElasticIsotropic parameters
# set v		[expr (3.*$K-2.*$G)/(2.*$G+6.*$K)];
# set E 		[expr 2.*(1.+$v)*$G];

source Material.tcl;

#===============================================
# CREATE ELEMENTS
#===============================================
set thick 	[expr 4.*$m];
set type 	PlaneStrain;

set pressure	0.0;
set rhoE 		0.0;
set b1 			0.0;
set b2 			[expr -1.0*$rhoM*$g];
set b3 			0.0;
# quad for 2d and bbarBrick for 3d

source Elements.tcl;

# puts "MODELLING DONE!!";						###
# puts "=========================";

#===============================================
# GRAVITY ANALYSIS
#===============================================
#set NstepG 1;									###
source Gravity.tcl;

#===============================================
# RAISE WATER
#===============================================
source RaiseWater.tcl;

#===============================================
# MODAL ANALYSIS
#===============================================
# set plotmodes no; #yes or no
# set modes 4;
# source Modes.tcl;

# puts "Modal Analysis Results Post - Gravity"
# puts "---------------------------------------"
# for {set i 1} {$i <= $modes} {incr i 1} {
	# puts "T$i = [lindex $T [expr $i-1]]";
# }
# puts "---------------------------------------"

#=================================================
# EARTHQUAKE
#=================================================
# set GMFile "GroundMotionFile.txt";
# set GMfactor [expr 2.0*$g];
set GMdirection 1;

# set dataGM [open $GMFile "r"];
# set GMDataAll [read $dataGM]; 
# close $dataGM;
# set NPTS 2000;
# set dt 0.02;

# DT and DURATION of Analysis
# set DtAnalysis	[expr $dt/2.0];
# set TmaxAnalysis	[expr $dt*$NPTS];

# DAMPING
# set w1 [lindex $omega $DampModei];
# set w2 [lindex $omega $DampModej];

# set ksi 0.05;
# set alphaM    [expr 2.*$ksi*$w1*$w2/($w1+$w2)];	
# set betaKinit [expr 2.*$ksi/($w1+$w2)];         
set betaKcurr 0.0;
set betaKcomm 0.0;
set alphaM    0.609871227400010;
set betaKinit 1.332420549603170e-04;
rayleigh $alphaM $betaKcurr $betaKinit $betaKcomm

set Display no;

source EQ.tcl;

#close $ExportID;									###
#close $ExportPA;									###

#wipe;