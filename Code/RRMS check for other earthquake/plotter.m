
%% Disp
nResp = 4;
nP = 1500;

cd(sprintf('TrueResults'));
TDresponse = zeros(nP,nResp);
for k = 1:nResp
    temp = load(sprintf('(True)Disp%d.txt',k));
    TDresponse(:,k) = temp(:,2);
end
cd('..');

cd(sprintf('InitialResults'));
IDresponse = zeros(nP,nResp);
for k = 1:nResp
    temp = load(sprintf('(Initial)Disp%d.txt',k));
    IDresponse(:,k) = temp(:,2);
end
cd('..');


cd(sprintf('At2secResults'));
D2response = zeros(nP,nResp);
for k = 1:nResp
    temp = load(sprintf('(At2sec)Disp%d.txt',k));
    D2response(:,k) = temp(:,2);
end
cd('..');

cd(sprintf('At3secResults'));
D3response = zeros(nP,nResp);
for k = 1:nResp
    temp = load(sprintf('(At3sec)Disp%d.txt',k));
    D3response(:,k) = temp(:,2);
end
cd('..');

cd(sprintf('At10secResults'));
D10response = zeros(nP,nResp);
for k = 1:nResp
    temp = load(sprintf('(At10sec)Disp%d.txt',k));
    D10response(:,k) = temp(:,2);
end
cd('..');


cd(sprintf('FinalResults'));
FDresponse = zeros(nP,nResp);
for k = 1:nResp
    temp = load(sprintf('(Final)Disp%d.txt',k));
    FDresponse(:,k) = temp(:,2);
end
cd('..');

sqrt(sum((TDresponse - IDresponse).^2)./sum((TDresponse).^2))*100
RRMSD = sqrt(sum((TDresponse - FDresponse).^2)./sum((TDresponse).^2))*100
%%
hFig = figure(1);
set(hFig, 'Position', [0 0 1500 700])
PointName = {'A','B','C','D'};
time = 0.01:0.01:15;
RESP = [IDresponse(:,1) D2response(:,1) D10response(:,1) FDresponse(:,1)];
tk = [0,3,10,20];
for i = 1:4
    subplot(2,2,i)
    plot(time,TDresponse(:,1),'g','linewidth',1.2);
    hold on;
    plot(time,RESP(:,i),'--k','linewidth',1.1);
    grid on;
    
    ylim([-5,10])
    legend({'True Resp.',['Predicted Resp. (using \theta_{k|k} at t_{k} = ' num2str(tk(i))  'sec)']});
    RRMS = sqrt(sum((TDresponse(:,1) - RESP(:,i)).^2)./sum((TDresponse(:,1)).^2))*100;
    
    title(sprintf('RRMS error: %4.2f%%',RRMS))
    xlabel('Time [sec]');
    ylabel('Rel. Disp. [in]');
    set(gca, 'fontsize',10.5);
end

%% Accel
nResp = 4;
nP = 1500;

GM = load('GM.txt');

cd(sprintf('TrueResults'));
TAresponse = zeros(nP,nResp);
for k = 1:nResp
    temp = load(sprintf('(True)Acc%d.txt',k));
    TAresponse(:,k) = temp(:,2)+GM;
end
cd('..');

cd(sprintf('InitialResults'));
IAresponse = zeros(nP,nResp);
for k = 1:nResp
    temp = load(sprintf('(Initial)Acc%d.txt',k));
    IAresponse(:,k) = temp(:,2)+GM;
end
cd('..');

cd(sprintf('FinalResults'));
FAresponse = zeros(nP,nResp);
for k = 1:nResp
    temp = load(sprintf('(Final)Acc%d.txt',k));
    FAresponse(:,k) = temp(:,2)+GM;
end
cd('..');

sqrt(sum((TAresponse - IAresponse).^2)./sum((TAresponse).^2))*100
RRMSA = sqrt(sum((TAresponse - FAresponse).^2)./sum((TAresponse).^2))*100

figure
PointName = {'A','B','C','D'};
time = 0.01:0.01:15;
for i = 1:4
    subplot(4,1,i)
    plot(time,TAresponse(:,i),'k');
    hold on;
    plot(time,FAresponse(:,i),'--r');
    grid on;
    title(sprintf('Point %s [RRMS: %4.2f%%]',PointName{i},RRMSA(i)),'FontSize',7)
end

%% Push Over
cd 'TrueResults';
Tpush = load('(True)ForceDisp.txt');
cd('..');

cd 'FinalResults';
Fpush = load('(Final)ForceDisp.txt');
cd('..');

cd 'InitialResults';
Ipush = load('(Initial)ForceDisp.txt');
cd('..');

% cd 'InitialResultsTGTK';
% ITGTKpush = load('(InitialTGTK)ForceDisp.txt');
% cd('..');

figure
plot(Ipush(:,2),Ipush(:,1),'k','linewidth',1.5)
hold on;
plot(Fpush(:,2),Fpush(:,1),'r','linewidth',1.5)
plot(Tpush(:,2),Tpush(:,1),'b','linewidth',1.5)
grid on;
xlabel('Horizontal Disp [inch]','FontSize',14);
ylabel('Force [kip]','FontSize',14);
legend({'Estimated (prior)','Estimated (posterior)','True'},'FontSize',10);
title('Force-Displacement','FontSize',14);

%plot(ITGTKpush(:,2),ITGTKpush(:,1))

% Error Plot
figure
plot(Tpush(:,2),Tpush(:,1)-Fpush(:,1),'r','linewidth',1.5);
grid on;
xlabel('Horizontal Disp [inch]','FontSize',14);
ylabel('Error [kip]','FontSize',14);
title('Error btw True & Estimate (posterior)','FontSize',14);

figure
plot(Tpush(:,2),((Tpush(:,1)-Fpush(:,1))./Tpush(:,1))*100,'b','linewidth',1.5);
grid on;
xlabel('Horizontal Disp [inch]','FontSize',14);
ylabel('Error [%]','FontSize',14);
title('Error btw True & Estimate (posterior)','FontSize',14);


%% Stress Path

cd 'TrueResults';
for n = 1:6
    khp{n} = load(sprintf('(True)k%d.txt',n));
    time = khp{n}(:,1);
    khp{n} = khp{n}(:,2);
    
    stress{n} = load(sprintf('(True)stress%d.txt',n));
    stress{n} = stress{n}(:,2:end);
    
    strain{n} = load(sprintf('(True)strain%d.txt',n));
    strain{n} = strain{n}(:,2:end);
    
    % hydrostatic stress tensor
    TepsI1(:,n) = strain{n}(:,1)+strain{n}(:,2)+strain{n}(:,3);
    
    % hydrostatic stress tensor
    TI1(:,n) = stress{n}(:,1)+stress{n}(:,2)+stress{n}(:,3);
    % deviatoric stress tensor
    S{n} = stress{n} - [TI1(:,n)./3 TI1(:,n)./3 TI1(:,n)./3 zeros(length(TI1(:,n)),1) zeros(length(TI1(:,n)),1) zeros(length(TI1(:,n)),1)];
    % frobenius norm of deviatoric stress tensor
    TNormS(:,n) = (S{n}(:,1).*S{n}(:,1)+S{n}(:,2).*S{n}(:,2)+...
        S{n}(:,3).*S{n}(:,3)+2*S{n}(:,4).*S{n}(:,4)+...
        2*S{n}(:,5).*S{n}(:,5)+2*S{n}(:,6).*S{n}(:,6)).^(0.5);
end
cd('..');

cd 'FinalResults';
for n = 1:6
    khp{n} = load(sprintf('(Final)k%d.txt',n));
    time = khp{n}(:,1);
    khp{n} = khp{n}(:,2);
    
    stress{n} = load(sprintf('(Final)stress%d.txt',n));
    stress{n} = stress{n}(:,2:end);
    
    strain{n} = load(sprintf('(Final)strain%d.txt',n));
    strain{n} = strain{n}(:,2:end);
    
    % hydrostatic stress tensor
    FepsI1(:,n) = strain{n}(:,1)+strain{n}(:,2)+strain{n}(:,3);
    
    % hydrostatic stress tensor
    FI1(:,n) = stress{n}(:,1)+stress{n}(:,2)+stress{n}(:,3);
    % deviatoric stress tensor
    S{n} = stress{n} - [FI1(:,n)./3 FI1(:,n)./3 FI1(:,n)./3 zeros(length(FI1(:,n)),1) zeros(length(FI1(:,n)),1) zeros(length(FI1(:,n)),1)];
    % frobenius norm of deviatoric stress tensor
    FNormS(:,n) = (S{n}(:,1).*S{n}(:,1)+S{n}(:,2).*S{n}(:,2)+...
        S{n}(:,3).*S{n}(:,3)+2*S{n}(:,4).*S{n}(:,4)+...
        2*S{n}(:,5).*S{n}(:,5)+2*S{n}(:,6).*S{n}(:,6)).^(0.5);
end
cd('..');

cd 'InitialResults';
for n = 1:6
    khp{n} = load(sprintf('(Initial)k%d.txt',n));
    time = khp{n}(:,1);
    khp{n} = khp{n}(:,2);
    
    stress{n} = load(sprintf('(Initial)stress%d.txt',n));
    stress{n} = stress{n}(:,2:end);
    
    strain{n} = load(sprintf('(Initial)strain%d.txt',n));
    strain{n} = strain{n}(:,2:end);
    
    % hydrostatic stress tensor
    IepsI1(:,n) = strain{n}(:,1)+strain{n}(:,2)+strain{n}(:,3);
    
    % hydrostatic stress tensor
    II1(:,n) = stress{n}(:,1)+stress{n}(:,2)+stress{n}(:,3);
    % deviatoric stress tensor
    S{n} = stress{n} - [II1(:,n)./3 II1(:,n)./3 II1(:,n)./3 zeros(length(II1(:,n)),1) zeros(length(II1(:,n)),1) zeros(length(II1(:,n)),1)];
    % frobenius norm of deviatoric stress tensor
    INormS(:,n) = (S{n}(:,1).*S{n}(:,1)+S{n}(:,2).*S{n}(:,2)+...
        S{n}(:,3).*S{n}(:,3)+2*S{n}(:,4).*S{n}(:,4)+...
        2*S{n}(:,5).*S{n}(:,5)+2*S{n}(:,6).*S{n}(:,6)).^(0.5);
end
cd('..');

sqrt(sum((TI1 - II1).^2)./sum((TI1).^2))*100
RRMSI1 = sqrt(sum((TI1 - FI1).^2)./sum((TI1).^2))*100

figure
PointName = {'A','B','C','D','E','F'};
time = 0.01:0.01:15;
for i = 1:6
    subplot(6,1,i)
    plot(time,TI1(:,i),'k');
    hold on;
    plot(time,FI1(:,i),'--r');
    grid on;
    title(sprintf('Point %s [RRMS: %4.2f%%]',PointName{i},RRMSI1(i)),'FontSize',7)
end

sqrt(sum((TNormS - INormS).^2)./sum((TNormS).^2))*100
RRMSNormS = sqrt(sum((TNormS - FNormS).^2)./sum((TNormS).^2))*100

figure
PointName = {'A','B','C','D','E','F'};
time = 0.01:0.01:15;
for i = 1:6
    subplot(6,1,i)
    plot(time,TNormS(:,i),'k');
    hold on;
    plot(time,FNormS(:,i),'--r');
    grid on;
    title(sprintf('Point %s [RRMS: %4.2f%%]',PointName{i},RRMSNormS(i)),'FontSize',7)
end

sqrt(sum((TepsI1 - IepsI1).^2)./sum((TepsI1).^2))*100
RRMSepsI1 = sqrt(sum((TepsI1 - FepsI1).^2)./sum((TepsI1).^2))*100
aaa
figure
PointName = {'A','B','C','D','E','F'};
time = 0.01:0.01:15;
for i = 1:6
    subplot(6,1,i)
    plot(time,TepsI1(:,i),'k');
    hold on;
    plot(time,FepsI1(:,i),'--r');
    grid on;
    title(sprintf('Point %s [RRMS: %4.2f%%]',PointName{i},RRMSepsI1(i)),'FontSize',7)
end