#===============================================
# CREATE NODES and ADD BOUNDARY CONDITIONS
#===============================================
set nodeTag 1;

foreach zvalue $Zcord {
	set index 0;
	foreach yvalue $Ycord {
		foreach xvalue [lindex $Xcord $index] {
			if {$ndm==2} {
				#node $nodeTag (ndm $coords) <-mass (ndf $massValues)>
				node $nodeTag $xvalue $yvalue;
				#puts $ExportID "node $nodeTag $xvalue $yvalue"
			} else {
				#node $nodeTag (ndm $coords) <-mass (ndf $massValues)>
				node $nodeTag $xvalue $yvalue $zvalue;
				#puts $ExportID "node $nodeTag $xvalue $yvalue $zvalue"
			}
			set nodeTag [expr $nodeTag+1];
		}
		set index [expr $index+1];
	}
}

set TotalNodes [expr $nodeTag-1];
set zlength [llength $Zcord];
set NumNodesPlane [expr $TotalNodes/$zlength];

#Fix BC to all base nodes only
set nodeTag 1;
foreach zvalue $Zcord {
	set index 0;
	foreach yvalue $Ycord {
		foreach xvalue [lindex $Xcord $index] {
			if {$yvalue==0.0} {
				if {$ndm==2} {
					#fix $nodeTag (ndf $constrValues)
					fix $nodeTag 1 1;
					#puts $ExportID "fix $nodeTag 1 1"
				} else {
					#fix $nodeTag (ndf $constrValues)
					fix $nodeTag 1 1 1;
					#puts $ExportID "fix $nodeTag 1 1 1"
				}
			}
			set nodeTag [expr $nodeTag+1];
		}
		set index [expr $index+1];
	}
}