#=================================================
# CREATE ELEMENTS
#=================================================

if {$ndm==2} {
	set eleTag 1;
	for {set y 1} {$y<=[expr $NumYBlocksBot+$NumYBlocksTop]} {incr y} {
		
		for {set x 1} {$x<=$NumXBlocks} {incr x} {
			set iNode 	[expr $x+($y-1)*($NumXBlocks+1)];
			set jNode	[expr $iNode+1];
			set lNode	[expr $x+($y)*($NumXBlocks+1)];
			set kNode 	[expr $lNode+1];
			
			#element quad $eleTag $iNode $jNode $kNode $lNode $thick $type $matTag <$pressure $rho $b1 $b2>
			element quad $eleTag $iNode $jNode $kNode $lNode $thick $type $matTag $pressure $rhoE $b1 $b2;
			#puts $ExportID "element quad $eleTag $iNode $jNode $kNode $lNode $thick $type $matTag $pressure $rhoE $b1 $b2";
			set eleTag [expr $eleTag+1];
		}
	}
} else {
	set eleTag 1;
	for {set z 1} {$z<=[expr $zlength-1]} {incr z} {
		for {set y 1} {$y<=[expr $NumYBlocksBot+$NumYBlocksTop]} {incr y} {
			for {set x 1} {$x<=$NumXBlocks} {incr x} {
			
				set node1 	[expr $x+($y-1)*($NumXBlocks+1)+($z-1)*($NumNodesPlane)];
				set node2	[expr $node1+1];
				set node4 	[expr $x+($y-1)*($NumXBlocks+1)+($z)*($NumNodesPlane)];
				set node3	[expr $node4+1];
				
				set node5	[expr $x+($y)*($NumXBlocks+1)+($z-1)*($NumNodesPlane)];
				set node6 	[expr $node5+1];
				set node8	[expr $x+($y)*($NumXBlocks+1)+($z)*($NumNodesPlane)];
				set node7	[expr $node8+1];
				
				#element bbarBrick $eleTag $node1 $node2 $node3 $node4 $node5 $node6 $node7 $node8 $matTag <$b1 $b2 $b3>
				element bbarBrick $eleTag $node1 $node2 $node3 $node4 $node5 $node6 $node7 $node8 $matTag $b1 $b2 $b3
				#puts $ExportID "element bbarBrick $eleTag $node1 $node2 $node3 $node4 $node5 $node6 $node7 $node8 $matTag $b1 $b2 $b3";
				set eleTag [expr $eleTag+1];
			}
		}
	}
}

set TotalElements [expr $eleTag-1];