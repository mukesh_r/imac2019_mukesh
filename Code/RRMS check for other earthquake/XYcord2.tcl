#===============================================
# CREATE Xcordinates and Ycoordinates
#===============================================

set Ycord {};
set currentheight 0.0;
for {set y 1} {$y<=[expr $NumYBlocksBot+$NumYBlocksTop+1]} {incr y} {
	
	lappend Ycord $currentheight;
	
	if {$y<=$NumYBlocksBot} {
		set Yincrement [expr ($height-$topHei)/$NumYBlocksBot];
	} else {
		set Yincrement [expr $topHei/$NumYBlocksTop];
	}
	
	set currentheight [expr $currentheight+$Yincrement];
};

set RoC [expr 50.*$ft];
set theta [expr atan(($height-$topHei)/($base-$top))+($pi/2.)];
set region2 [expr $RoC*cos($theta/2.)/cos(($pi-$theta)/2.)];
set region1 [expr $region2*cos($pi-$theta)];

set Xcord {};
set xwidth $base;
set xwidthvirtual $base;
set index 1;
foreach yvalue $Ycord {
	
	set temp {};
	for {set x 1} {$x<=$NumXBlocks+1} {incr x} {
		lappend temp [expr ($x-1)*($xwidth/$NumXBlocks)];
	};
	
	lappend Xcord $temp;
	
	set ynext [lindex $Ycord $index];
	
	if {$HandleIrregularity == "ChangeEntireDam"} {
	
		if {$index<=$NumYBlocksBot} {
			set Xdecrement [expr ($ynext-$yvalue)*($base-$topbot)/($height-$topHei)];
		} else {
			set Xdecrement [expr ($ynext-$yvalue)*($topbot-$top)/($topHei)];
		}
		
		set xwidth [expr $xwidth-$Xdecrement];
		
	} elseif {$HandleIrregularity == "JustChangeNode"} {
	
		if {$index<$NumYBlocksBot} {
			set Xdecrement [expr ($ynext-$yvalue)*($base-$top)/($height-$topHei)];
		} elseif {$index==$NumYBlocksBot} {
			set Xdecrement [expr (($ynext-$yvalue)*($base-$top)/($height-$topHei))-($topbot-$top)];
		} elseif {$index==[expr $NumYBlocksBot+1]} {
			set Xdecrement [expr ($topbot-$top)];
		} else {
			set Xdecrement 0.0;
		}
		
		set xwidth [expr $xwidth-$Xdecrement];

	} elseif {$HandleIrregularity == "Curve"} {
	
		if {$ynext<=[expr $height-$topHei-$region1]} {
			set Xdecrement [expr ($ynext-$yvalue)*($base-$top)/($height-$topHei)];
			
			set duetocurve 0.0;
		} elseif {$index<=$NumYBlocksBot} { 
			#$ynext<=[expr $height-$topHei]
			set Xdecrement [expr ($ynext-$yvalue)*($base-$top)/($height-$topHei)];
			
			set xfromreg2top [expr $height-$topHei+$region2-$ynext];
			set angle [expr asin($xfromreg2top/$RoC)];
			set xfrompoint [expr $height-$topHei-$ynext];
			set duetocurve [expr ((2.*$RoC*sin($angle/2.)*sin($angle/2.))-($xfrompoint*tan($pi-$theta)))];
		} elseif {$ynext<=[expr $height-$topHei+$region2]} {
			set Xdecrement 0.0;
		
			set xfromreg2top [expr $height-$topHei+$region2-$ynext];
			set angle [expr asin($xfromreg2top/$RoC)];
			set duetocurve [expr (2.*$RoC*sin($angle/2.)*sin($angle/2.))];
		} else {
			set Xdecrement 0.0;
			
			set duetocurve 0.0;
		}
		set xwidthvirtual [expr $xwidthvirtual-$Xdecrement];
		set xwidth [expr $xwidthvirtual+$duetocurve];
	}
	
	set index [expr $index+1];
}