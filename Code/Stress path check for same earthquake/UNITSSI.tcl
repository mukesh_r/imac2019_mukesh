
# UNITS -----------------------------------------------------------------------------
set m 		1.; 			# define basic units -- output units
set N	 	1.; 			# define basic units -- output units
set sec 	1.; 			# define basic units -- output units

set ft 		[expr 0.3048*$m];
set in		[expr 12.*$ft];

set psi		[expr 6894.76*$N/pow($m,2)];
set ksi		[expr 1000.*$psi];
set MPa 	[expr 1.0e6*$N/pow($m,2)];
set GPa		[expr 1.0e9*$N/pow($m,2)];

set g 		[expr 9.81*$m/pow($sec,2)];

set pi 		[expr 2*asin(1.0)];

# puts "BASIC UNITS - m, N, sec"
# puts "=========================";