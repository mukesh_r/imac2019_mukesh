close all; clear all; clc;
tic
%profile on
%parpool(4);
%% To OpenSees
UNITS = 'US';
materialInput.matType = 'CapPlasticity';
materialInput.matDensity = 2.2436099307234795e-7;
materialInput.matTolerance = 1.0e-6;
ParameterInfo.ParameterName = {'G','K','X','D','W','R','lambda','theta','beta','alpha','T'};
ParameterInfo.TrueParameterValues = [1700; 2100; 16; 0.0032; 0.42; 4.43; 1.16; 0.11; 0.441; 3.86; -0.3];
NstepG = 1;
% GMinput.GMFile = 'GroundMotionFile.txt';
GMinput.GMfactor = 2.0;
% GMinput.GMdirection = 1;
GMinput.DtAnalysis = 0.01;
GMinput.TmaxAnalysis = 20.0;
GMinput.Nsteps = floor(GMinput.TmaxAnalysis/GMinput.DtAnalysis);
% DampingInput.DampModei = 0;
% DampingInput.DampModej = 1;
% DampingInput.ksi = 0.05;
g = 32.2*12; %in/s2

%% Modify GM file
GMinput.TrueGM = ModifyGM(GMinput,g);

%% For filtering
f_state_eqn = @(x) x;
NumData = GMinput.Nsteps;
StepUpdateSize = 10;
UpdateStep = StepUpdateSize:StepUpdateSize:NumData;
TotalUpdateSteps = length(UpdateStep);

%% Parameters to Update
UpdateParameterName = {'G','K','X','D','W','R','lambda','theta','beta','alpha','T'}; %maintain the order
ParameterInfo.UpdateParameterIndex = ismember(ParameterInfo.ParameterName,UpdateParameterName);
TrueUpdateParameterValues = ParameterInfo.TrueParameterValues(ParameterInfo.UpdateParameterIndex);
TrueNotUpdateParameterValues = ParameterInfo.TrueParameterValues(~ParameterInfo.UpdateParameterIndex);
ntheta = length(UpdateParameterName);

%% Prior Knowledge
xcap_0_0 = [1.40; 0.55; 0.85; 1.20; 0.80; 0.80; 0.85; 1.05; 0.95; 0.80; 0.60];%.*TrueUpdateParameterValues;
covx_0_0 = [0.30; 0.30; 0.15; 0.20; 0.20; 0.20; 0.20; 0.05; 0.10; 0.20; 0.30];
Pxxcap_0_0 = (covx_0_0).^2; %diagonal entries only

% constraints
Constraints.constrainFlag = 'no';
% Constraints.Lower = [0.1; 0.1; 0.1; 0.1; 0.1; 0.1; 0.1; 0.1; 0.1; 0.1; 2];%.*TrueUpdateParameterValues;
% Constraints.Upper = [2; 2; 2; 2; 2; 2; 2; 2; 2; 2; 0.1];%.*TrueUpdateParameterValues;
% 
% Constraints.PreLower = [0.05; 0.05; 0.05; 0.05; 0.05; 0.05; 0.05; 0.05; 0.05; 0.05; 2.5];%.*TrueUpdateParameterValues;
% Constraints.PreUpper = [2.5; 2.5; 2.5; 2.5; 2.5; 2.5; 2.5; 2.5; 2.5; 2.5; 0.05];%.*TrueUpdateParameterValues;

% not updating parameters
%1's if you want to use true
ParameterInfo.NotUpdatingParametersValues = [].*TrueNotUpdateParameterValues; %maintain the order
ParameterInfo.TrueUpdateParameterValues = TrueUpdateParameterValues;
%% plot prior knowledge cap
[I1f1,NormSf1,I1f2,NormSf2,I1f3,NormSf3] = CapDataToPlot(ParameterInfo,xcap_0_0.*TrueUpdateParameterValues);

figure
plot(I1f1,NormSf1,'r','linewidth',1.5);
hold on;
plot(I1f2,real(NormSf2),'color',[0 176/255 80/255],'linewidth',1.5);
plot(I1f3,NormSf3,'b','linewidth',1.5);

[I1f1,NormSf1,I1f2,NormSf2,I1f3,NormSf3] = CapDataToPlot(ParameterInfo,ones(ntheta,1).*TrueUpdateParameterValues);

plot(I1f1,NormSf1,'k','linewidth',1);
hold on;
plot(I1f2,real(NormSf2),'k','linewidth',1);
plot(I1f3,NormSf3,'k','linewidth',1);

grid on;
xlabel('I_1 [ksi]','FontSize',14);
ylabel('||S|| [ksi]','FontSize',14);
title('Prior Knowledge','FontSize',14);
%% Process Noise (x_k = x_km1 + w_km1)
% w_km1 ~ N(0,Q_km1)
q = 1*10^(-3);
Q = (q^2)*ones(length(xcap_0_0),1); %diagonal entries only

%% Add Noise to GMs
RMS_InputPolluteNoise = 1.0*g/100;
rng(1000);
InputNoise = RMS_InputPolluteNoise*randn(NumData,1);
GMinput.NoisyGM = GMinput.TrueGM + InputNoise;

fileID = fopen('NoisyGM.txt','w');
fprintf(fileID,'%.15f\n',GMinput.NoisyGM);
fclose(fileID);

%% Noise for polluting data
nResp = [7; 7]; %Accel; Disp
ny = sum(nResp); %number of measurements
RMS_PolluteNoise = [1.0*g/100; 0.075]; % [in/s2; in]
%CoVar_PolluteNoise = (RMS_PolluteNoise^2)*ones(ny,1); %diagonal entries only

% True parameters and True GM
TrueResponse = runFEM(ParameterInfo.TrueParameterValues,UNITS,materialInput,ParameterInfo.ParameterName,NstepG,GMinput,nResp,'True','GM.txt');
NormalizeWith = max(abs(TrueResponse));
NormalizedTrueResponse = TrueResponse./NormalizeWith;

RMS_PolluteNoise_Channel = repelem(RMS_PolluteNoise,nResp);
MeasurementNoise = zeros(NumData,ny);
for i = 1:ny
    rng(100+i);
    MeasurementNoise(:,i) = RMS_PolluteNoise_Channel(i)*randn(1,NumData);
end
TrueMNResponse = TrueResponse + MeasurementNoise; %mimics the real world data
NormalizedTrueMNResponse = TrueMNResponse./NormalizeWith;

figure
plot(MeasurementNoise(:,1));
hold on;
plot(TrueResponse(:,1))
plot(TrueMNResponse(:,1))
RRMSTrueAndNoisyTrue = sqrt(sum((TrueResponse - TrueMNResponse).^2)./sum((TrueResponse).^2))*100;
disp(['True vs Noisy True RRMS:  ' num2str(RRMSTrueAndNoisyTrue)]);

%% Quantify Noise (interms of %g)
% True parameters and Noisy GM
TrueINResponse = runFEM(ParameterInfo.TrueParameterValues,UNITS,materialInput,ParameterInfo.ParameterName,NstepG,GMinput,nResp,'TrueIN','NoisyGM.txt');

TrueINMNResponse = TrueINResponse + MeasurementNoise; 
TotalNoise = (TrueResponse - TrueINMNResponse);
RMS_MeasuementNoiseEstimate = std(TotalNoise);
disp(['RMS_MeasuementNoiseEstimate:  ' num2str(std(TotalNoise))]);
RMS_NormalizedMeasuementNoiseEstimate = std(TotalNoise./NormalizeWith);
disp(['RMS_NormalizedMeasuementNoiseEstimate:  ' num2str(std(TotalNoise./NormalizeWith))]);

%% Measurement Noise (y_k = h_k(x_k) + v_k)
% vk ~ N(0,Rk)
RMS_MeasuementNoise = 1.0*RMS_NormalizedMeasuementNoiseEstimate;
R = repmat(RMS_MeasuementNoise.^2,[1,StepUpdateSize])';
%R = (RMS_MeasuementNoise^2)*ones(StepUpdateSize*ny,1); %diagonal entries only

%% UKF PARAMETERS
SPsPar.UTflag = 'SUT';
SPsPar.nz = length(UpdateParameterName);

% % UT
% SPsPar.kappa = 1;
% SPsPar.gamma = sqrt(SPsPar.nz+SPsPar.kappa);

% Scaled UT
SPsPar.alpha = 0.01;
SPsPar.kappa = 0;
SPsPar.beta = 2;

SPsPar.lambda = (SPsPar.alpha^2)*(SPsPar.nz+SPsPar.kappa) - SPsPar.nz;
SPsPar.gamma = sqrt(SPsPar.nz+SPsPar.lambda);

% % % % % weight coefficients of SPs to estimate mean of s: muS
% % % % Wm0 = SPsPar.lambda/(SPsPar.nz+SPsPar.lambda);
% % % % Wmi = (1/(2*(SPsPar.nz+SPsPar.lambda)))*ones(1,2*SPsPar.nz);
% % % % SPsPar.Wm = [Wm0 Wmi]; %(1 x 2ntheta+1)
% % % % 
% % % % % weight coefficients of SPs to estimate covariance of s: Pss
% % % % Wc0 = (SPsPar.lambda/(SPsPar.nz+SPsPar.lambda))+(1-SPsPar.alpha^2+SPsPar.beta);
% % % % Wci = (1/(2*(SPsPar.nz+SPsPar.lambda)))*ones(1,2*SPsPar.nz);
% % % % SPsPar.Wc = [Wc0 Wci]; %(1 x 2ntheta+1)

%% Initial RRMS
% Initial Response using prior knowledge
InitialPar = zeros(length(ParameterInfo.ParameterName),1);
InitialPar(ParameterInfo.UpdateParameterIndex) = xcap_0_0.*TrueUpdateParameterValues;
InitialPar(~ParameterInfo.UpdateParameterIndex) = ParameterInfo.NotUpdatingParametersValues;
% Intial parameters and True GM
InitialResponse = runFEM(InitialPar,UNITS,materialInput,ParameterInfo.ParameterName,NstepG,GMinput,nResp,'Initial','GM.txt');
RRMSInitial = sqrt(sum((TrueResponse - InitialResponse).^2)./sum((TrueResponse).^2))*100;
disp(['Initial RRMS:  ' num2str(RRMSInitial)]);

%% Filtering
xcap = zeros(ntheta,TotalUpdateSteps+1); %last is the posterior after all updating
Pxxcap = zeros(ntheta,ntheta,TotalUpdateSteps+1);

xcap(:,1) = xcap_0_0;
Pxxcap(:,:,1) = diag(Pxxcap_0_0);

disp(['Parameter at  ' num2str((xcap(:,1))')]);

for UpdateNum = 1:TotalUpdateSteps
    GMinput.Nsteps = UpdateStep(UpdateNum);
    NormalizedTrueMNResponseMatrix = NormalizedTrueMNResponse(UpdateStep(UpdateNum)-StepUpdateSize+1:UpdateStep(UpdateNum),:);
    NormalizedTrueMNResponseVector = reshape(NormalizedTrueMNResponseMatrix',[StepUpdateSize*ny,1]);
    
    fprintf('UpdateNum: %3.0f.......UpdateStep: %3.0f.......Progress: %4.2f%% Done \n',UpdateNum,UpdateStep(UpdateNum),UpdateNum*100/length(UpdateStep));
    [xcap(:,UpdateNum+1),Pxxcap(:,:,UpdateNum+1)] = UKF(xcap(:,UpdateNum), Pxxcap(:,:,UpdateNum), diag(Q), diag(R), NormalizedTrueMNResponseVector, f_state_eqn,SPsPar,UNITS,materialInput,ParameterInfo,NstepG,GMinput,StepUpdateSize,nResp,Constraints,NormalizeWith);
    disp(['Parameter at  ' num2str((xcap(:,UpdateNum+1))')]);
    disp(['StdDev    at  ' num2str(sqrt(diag(Pxxcap(:,:,UpdateNum+1)))')]);
    fprintf('\n');
end

%% Final RRMS
GMinput.Nsteps = floor(GMinput.TmaxAnalysis/GMinput.DtAnalysis);
% Final Response using Posterior knowledge
FinalPar = zeros(length(ParameterInfo.ParameterName),1);
FinalPar(ParameterInfo.UpdateParameterIndex) = xcap(:,100).*TrueUpdateParameterValues;
FinalPar(~ParameterInfo.UpdateParameterIndex) = ParameterInfo.NotUpdatingParametersValues;
% Final parameters and True GM
FinalResponse = runFEM(FinalPar,UNITS,materialInput,ParameterInfo.ParameterName,NstepG,GMinput,nResp,'At10sec','GM.txt');
RRMSFinal = sqrt(sum((TrueResponse - FinalResponse).^2)./sum((TrueResponse).^2))*100;
disp(['Initial RRMS:  ' num2str(RRMSInitial)]);
disp(['Final RRMS:  ' num2str(RRMSFinal)]);

%% RRMS evolution with time
RRMS(1,:) = RRMSInitial;
disp(['Initial RRMS:  ' num2str(RRMSInitial)]);
SeqResponse(:,:,1) =  InitialResponse;

Par = zeros(length(ParameterInfo.ParameterName),1);
for UpdateNum = 2:TotalUpdateSteps
    Par(ParameterInfo.UpdateParameterIndex) = xcap(:,UpdateNum).*TrueUpdateParameterValues;
    Par(~ParameterInfo.UpdateParameterIndex) = ParameterInfo.NotUpdatingParametersValues;
    SeqResponse(:,:,UpdateNum) = runFEM(Par,UNITS,materialInput,ParameterInfo.ParameterName,NstepG,GMinput,nResp,'ForRRMS','GM.txt');
    RRMS(UpdateNum,:) = sqrt(sum((TrueResponse - SeqResponse(:,:,UpdateNum)).^2)./sum((TrueResponse).^2))*100;
    disp(['RRMS of UpdateNum ' num2str(UpdateNum) ' is  ' num2str(RRMS(UpdateNum,:))]);
end

RRMS(TotalUpdateSteps+1,:) = RRMSFinal;
disp(['Final RRMS:  ' num2str(RRMSFinal)]);
SeqResponse(:,:,TotalUpdateSteps+1) =  FinalResponse;

%%
figure
for i = 1:ny
    if i<=nResp(1) 
        c ='r';
    else
        c ='k';
    end
    time = [0 UpdateStep*GMinput.DtAnalysis];
    h(i) = plot(time,RRMS(:,i),'color',c);
    hold on;
end
xlim([min(time),max(time)]);
grid on;
xlabel('Time [sec]');
ylabel('RRMS error [%]');
%title('RRMS evolution','FontSize',14);
legend([h(nResp(1)),h(nResp(1)+1)],{'Absolute Acceleration Sensors [a_{A},a_{B},...,a_{F}]','Relative Displacement Sensors [d_{A},d_{B},...,d_{F}]'});

time = GMinput.DtAnalysis:GMinput.DtAnalysis:GMinput.TmaxAnalysis;
figure
for i = 60:TotalUpdateSteps+1
    h1 = plot(time,SeqResponse(:,1,i)/g,'color',[0.6 0.6 0.6]);
    hold on;
end
h2 = plot(time,TrueResponse(:,1)/g,'r');
xlim([min(time),max(time)]);
grid on;
xlabel('Time [sec]');
ylabel('Abs. Accel. [g]');
title('Accel. Response at top of the dam','FontSize',14);
legend([h2,h1],{'True','Seq. Realizations'});

%% plot
%hFig = figure(1);
%set(hFig, 'Position', [0 0 1000 700])
figure
count = 1;
for i = 1:ntheta
    time = [0 UpdateStep*GMinput.DtAnalysis];
    subplot(4,3,count)
    count = count + 1;
    
    sig = sqrt(Pxxcap(i,i,:));
    sig = reshape(sig,[1,TotalUpdateSteps+1]);
    X = [time fliplr(time)];
    Y1 = [(xcap(i,1:TotalUpdateSteps+1)+2*sig) fliplr((xcap(i,1:TotalUpdateSteps+1)-2*sig))];
    %Y2 = [(xcap(i,1:TotalUpdateSteps+1)+1*sig) fliplr((xcap(i,1:TotalUpdateSteps+1)-1*sig))];
    %Y3 = [(xcap(i,1:TotalUpdateSteps+1)+0.5*sig) fliplr((xcap(i,1:TotalUpdateSteps+1)-0.5*sig))];
    
    hold on;
    h2 = fill(X,Y1,[0.75 0.75 0.75],'LineStyle','none','facealpha',0.8);
    %h3 = fill(X,Y2,'-m','LineStyle','none','facealpha',0.5);
    %h4 = fill(X,Y3,'-b','LineStyle','none','facealpha',0.6);
    
    h1 = plot(time,xcap(i,1:TotalUpdateSteps+1),'b','linewidth',1.2);
    plot(time,ones(length(time),1),'k--','linewidth',1.2)
    grid on;
    
    if (i>=7) && (i<=10)
            ylabel(['\',UpdateParameterName{i},' / ','\',UpdateParameterName{i},'^{true}'],'FontAngle', 'italic');
    else
            ylabel([UpdateParameterName{i},' / ',UpdateParameterName{i},'^{true}'],'FontAngle', 'italic');
    end
    
    %ylabel([UpdateParameterName{i},' / ',UpdateParameterName{i},'_{true}']);
    %set(gca,'fontsize',12);
    %legend([h1,h2],{'\mu','[\mu-\sigma,\mu+\sigma]'})
    %xlim([0,3])
    ylim([min(Y1) max(Y1)]);
    ax=gca;
    ax.GridAlpha=0.4;
end
%xlabel('Time [sec]');

%% Identifiable parameters evolution
xCcap = [xcap(1,:); xcap(2,:); xcap(11,:); 
    (xcap(10,:)*3.86-xcap(7,:)*1.16)/(3.86-1.16);
    ((xcap(7,:).*xcap(9,:))*(1.16*0.441)+xcap(8,:)*0.11)/(1.16*0.441+0.11);
    xcap(4,:).*xcap(5,:);
    (xcap(3,:)*16-3.3282*xcap(6,:)*4.43)/1.25];
figure
count = 1;
for i = 1:size(xCcap,1)
    %time = [0 UpdateStep*GMinput.DtAnalysis];
    subplot(3,3,count)
    count = count + 1;
    
    plot(time,xCcap(i,1:TotalUpdateSteps+1))

end


%% PDF evolution
for i = 1 %ntheta
    figure
    hold on;
    for UpdateNum = 1:TotalUpdateSteps+1
        mu = xcap(i,UpdateNum);
        sigma = sqrt(Pxxcap(i,i,UpdateNum));
        
        x = mu-3*sigma:6*sigma/100:mu+3*sigma;
        y = normpdf(x,mu,sigma);
        
        plot(x,y);
        xlim([1500,2000]);
        %pause(0.1)
    end
end

%% CAP evolution
figure
xcap(:,TotalUpdateSteps+2) = ones(length(xcap_0_0),1);
for UpdateNum = [2:TotalUpdateSteps+2 1]
    
    [I1f1,NormSf1,I1f2,NormSf2,I1f3,NormSf3] = CapDataToPlot(ParameterInfo,xcap(:,UpdateNum).*TrueUpdateParameterValues);
    
    if UpdateNum == 1 % Prior
        col = [0 0 0];
        LW = 2;
    elseif UpdateNum == TotalUpdateSteps+1 % Posterior
        col = [1 0 0];
        LW = 2;
    elseif UpdateNum == TotalUpdateSteps+2 % True
        col = [0 0 1];
        LW = 1;
    else % Seq. Estimates
        col = [0.75 0.75 0.75];
        LW = 1;
    end
    
    h(UpdateNum) = plot(I1f1,NormSf1,'color',col,'linewidth',LW);
    hold on;
    plot(I1f2,real(NormSf2),'color',col,'linewidth',LW);
    plot(I1f3,NormSf3,'color',col,'linewidth',LW);
    %drawnow;
end
units = 'ksi';
grid on;
xlabel(sprintf('I_1 [%s]',units),'FontSize',14);
ylabel(sprintf('||s|| [%s]',units),'FontSize',14);
%title('Cap Plasticity Model','FontSize',14);
%legend([h(1) h(TotalUpdateSteps+1) h(TotalUpdateSteps+2) h(2)],{'Prior (mean)','Posterior (mean)','True','Seq. Estimates'},'FontSize',10);

%% Parameter evolution in 2D space
CorrPar = [1 2;3 6;4 5;7 10;8 9];
figure
count = 1;
for i = 1:size(CorrPar,1)
    if ParameterInfo.UpdateParameterIndex(CorrPar(i,1)) * ParameterInfo.UpdateParameterIndex(CorrPar(i,2)) == 1
        Par1 = ParameterInfo.ParameterName(CorrPar(i,1));
        Par2 = ParameterInfo.ParameterName(CorrPar(i,2));
        
        Par1Index = strmatch(Par1,UpdateParameterName);
        Par2Index = strmatch(Par2,UpdateParameterName);
        
        Par1true = ParameterInfo.TrueParameterValues(Par1Index);
        Par2true = ParameterInfo.TrueParameterValues(Par2Index);
        
        Par1hist = xcap(Par1Index,1:TotalUpdateSteps+1)*Par1true;
        Par2hist = xcap(Par2Index,1:TotalUpdateSteps+1)*Par2true;
        
        Par1sig = 2*reshape(sqrt(Pxxcap(Par1Index,Par1Index,:)),[1,TotalUpdateSteps+1])*Par1true;
        Par2sig = 2*reshape(sqrt(Pxxcap(Par2Index,Par2Index,:)),[1,TotalUpdateSteps+1])*Par2true;
        
        subplot(ceil(ntheta/4),2,count)
        errorbar(Par1hist(2:TotalUpdateSteps),Par2hist(2:TotalUpdateSteps),Par2sig(2:TotalUpdateSteps),Par2sig(2:TotalUpdateSteps),Par1sig(2:TotalUpdateSteps),Par1sig(2:TotalUpdateSteps),'o','color',[0.75 0.75 0.75]);
        hold on;
        errorbar(Par1hist(1),Par2hist(1),Par2sig(1),Par2sig(1),Par1sig(1),Par1sig(1),'ko');
        errorbar(Par1hist(TotalUpdateSteps+1),Par2hist(TotalUpdateSteps+1),Par2sig(TotalUpdateSteps+1),Par2sig(TotalUpdateSteps+1),Par1sig(TotalUpdateSteps+1),Par1sig(TotalUpdateSteps+1),'ro');
        plot(Par1true,Par2true,'bo','MarkerFaceColor','b','MarkerSize',5);
        grid on;
        if (CorrPar(i,1)>=7) && (CorrPar(i,1)<=10)
            xlabel(strcat('\',Par1),'FontSize',14);
        else
            xlabel(Par1,'FontSize',14);
        end
        if (CorrPar(i,2)>=7) && (CorrPar(i,2)<=10)
            ylabel(strcat('\',Par2),'FontSize',14);
        else
            ylabel(Par2,'FontSize',14);
        end
        count = count + 1;
    end
end

%% compare true and filtered response
figure
PointName = {'A','B','C','D','E','F','G'};
time = GMinput.DtAnalysis:GMinput.DtAnalysis:GMinput.TmaxAnalysis;
for i = 1:ny
    subplot(4,ceil(ny/4),i)
    plot(time,TrueResponse(:,i)/g,'k');
    hold on;
    plot(time,FinalResponse(:,i)/g,'--r');
    grid on;
    %legend({'True','Estimated'},'FontSize',10);
    %title(sprintf('Accel. Response Point %s',PointName{i}))
end
%xlabel('Time [sec]','FontSize',14);

runtime = toc;
disp(['Run Time: ' num2str(runtime/60),'min']);
profile viewer