function [response] =  runFEM(parameters,UNITS,materialInput,ParameterName,NstepG,GMinput,numResp,runType,GMFile)
    numPar = size(parameters,1);
    
    FileparID = fopen(sprintf('%spar.tcl',runType),'w');
    fprintf(FileparID,'%s\n','wipe;');
    fprintf(FileparID,'%s%s%s\n','set SPnum "',runType,'";');
    fprintf(FileparID,'%s%s%s\n','source UNITS',UNITS,'.tcl;');
    fprintf(FileparID,'%s%s%s\n','set matType ',materialInput.matType,';');
    fprintf(FileparID,'%s%e%s\n','set rhoM ',materialInput.matDensity,';');
    for j = 1:numPar
        fprintf(FileparID,'%s%s%s%f%s\n','set ',ParameterName{j},' ',parameters(j),';');
    end
    fprintf(FileparID,'%s%e%s\n','set tol ',materialInput.matTolerance,';');
    fprintf(FileparID,'%s%d%s\n','set NstepG ',NstepG,';');
    fprintf(FileparID,'%s%s%s\n','set GMFile "',GMFile,'";');
    %fprintf(FileparID,'%s%f%s\n','set GMfactor [expr ',GMinput.GMfactor,'*$g];');
    %fprintf(FileparID,'%s%d%s\n','set GMdirection ',GMinput.GMdirection,';');
    fprintf(FileparID,'%s%f%s\n','set DtAnalysis ',GMinput.DtAnalysis,';');
    fprintf(FileparID,'%s%d%s\n','set Nsteps ',GMinput.Nsteps,';');
    %fprintf(FileparID,'%s%d%s\n','set DampModei ',DampingInput.DampModei,';');
    %fprintf(FileparID,'%s%d%s\n','set DampModej ',DampingInput.DampModej,';');
    %fprintf(FileparID,'%s%f%s\n','set ksi ',DampingInput.ksi,';');
    fprintf(FileparID,'%s%s%s\n','set dataDir "',runType,'Results";');
    fprintf(FileparID,'%s\n','source runUKF.tcl;');
    fprintf(FileparID,'%s','wipe;');
    fclose(FileparID);
    [~,~] = system(['OpenSees_Cap ',runType,'par.tcl']);
    
    while 1
        status = load(sprintf('status(%s).txt',runType));
        if (status == 0 || materialInput.matTolerance == 1.0e-4) %run was successful
            materialInput.matTolerance = 1.0e-6;
            break;
        else %run was unsuccessful
            if materialInput.matTolerance == 1.0e-6
                materialInput.matTolerance = 1.0e-7;
                runagain(runType,materialInput.matTolerance);
            elseif materialInput.matTolerance == 1.0e-7
                materialInput.matTolerance = 1.0e-5;
                runagain(runType,materialInput.matTolerance);
            elseif materialInput.matTolerance == 1.0e-5
                materialInput.matTolerance = 1.0e-8;
                runagain(runType,materialInput.matTolerance);
            elseif materialInput.matTolerance == 1.0e-8
                materialInput.matTolerance = 1.0e-4;
                runagain(runType,materialInput.matTolerance);
            end
        end
    end
    
    GM = load(sprintf('%s',GMFile));

    cd(sprintf('%sResults',runType));
    response = zeros(GMinput.Nsteps,sum(numResp));
    % Accelerometers
    for k1 = 1:numResp(1)
        temp = load(sprintf('(%s)Acc%d.txt',runType,k1));
        response(:,k1) = temp(:,2)+GM;
    end
    % Displacement Sensors
    for k2 = 1:numResp(2)
        temp = load(sprintf('(%s)Disp%d.txt',runType,k2));
        response(:,numResp(1)+k2) = temp(:,2);
    end
    cd('..');
    
    function runagain(runType,tol)
        ChangeFileData = regexp( fileread(sprintf('%spar.tcl',runType)), '\n', 'split');
        ChangeFileData{17} = strcat("set tol ",num2str(tol,'%e'),";"); %17 is tolerance line
        fid = fopen(sprintf('%spar.tcl',runType), 'w');
        fprintf(fid, '%s\n', ChangeFileData{:});
        fclose(fid);
        [~,~] = system(['OpenSees_Cap ',runType,'par.tcl']);
    end
end