cd 'TrueResults';

stress = load('(True)stress.txt');
time = stress(:,1);
stress = stress(:,2:end);

% hydrostatic stress tensor
TI1 = stress(:,1)+stress(:,2)+stress(:,3);

% deviatoric stress tensor
S = stress - [TI1./3 TI1./3 TI1./3 zeros(length(TI1),1) zeros(length(TI1),1) zeros(length(TI1),1)];

% frobenius norm of deviatoric stress tensor
TNormS = (S(:,1).*S(:,1)+S(:,2).*S(:,2)+...
    S(:,3).*S(:,3)+2*S(:,4).*S(:,4)+...
    2*S(:,5).*S(:,5)+2*S(:,6).*S(:,6)).^(0.5);

cd('..');

cd 'FinalResults';

stress = load('(Final)stress.txt');
time = stress(:,1);
stress = stress(:,2:end);

% hydrostatic stress tensor
FI1 = stress(:,1)+stress(:,2)+stress(:,3);

% deviatoric stress tensor
S = stress - [FI1./3 FI1./3 FI1./3 zeros(length(FI1),1) zeros(length(FI1),1) zeros(length(FI1),1)];

% frobenius norm of deviatoric stress tensor
FNormS = (S(:,1).*S(:,1)+S(:,2).*S(:,2)+...
    S(:,3).*S(:,3)+2*S(:,4).*S(:,4)+...
    2*S(:,5).*S(:,5)+2*S(:,6).*S(:,6)).^(0.5);

cd('..');

hFig = figure(1);
set(hFig, 'Position', [0 0 1000 500])
subplot(2,1,1)
plot(time,TI1,'g','linewidth',1.2);
hold on;
plot(time,FI1,'--k','linewidth',1.1);
grid on;
xlim([0,20]);
xlabel('Time [sec]');
%ylabel('I_{1} [ksi]','FontAngle', 'italic');
legend({'True Response',['Predicted Resp. (using $\hat{theta}$ )']},'Interpreter','latex');

subplot(2,1,2)
plot(time,TNormS,'g','linewidth',1.2);
hold on;
plot(time,FNormS,'--k','linewidth',1.2);
grid on;
xlim([0,20]);
xlabel('Time [sec]');
%ylabel('||s|| [ksi]');
legend({'True Response',['Predicted Resp. (using \theta_{k|k} at t_{k} = 20sec)']});