#=================================================
# MODAL ANALYSIS
#=================================================

set omega {};
set F {};
set T {};
set PI [expr 2.0*asin(1.0)];
set lambda [eigen $modes];
foreach lam $lambda {
	lappend omega [expr sqrt($lam)];
	lappend F [expr sqrt($lam)/(2*$PI)];
	lappend T [expr (2.0*$PI)/sqrt($lam)];
};

#=================================================
# DATA TO PLOT MODES
#=================================================

if {$plotmodes == "yes"} {

	set ModeDir "Modal Analysis Results";
	file mkdir $ModeDir;

	if {$GravityAnalysis == "Done"} {
		set ModeSubDir "Post Gravity Analysis";
		file mkdir $ModeDir/$ModeSubDir;
	} else {
		set ModeSubDir "Pre Gravity Analysis";
		file mkdir $ModeDir/$ModeSubDir;
	}

	set TimePeriodFile [open "$ModeDir/$ModeSubDir/TimePeriods.txt" "w"];
	puts $TimePeriodFile "$T";
	close $TimePeriodFile;

	for {set i 1} {$i <= $modes} {incr i 1} {
		set ModeFile [open "$ModeDir/$ModeSubDir/Mode$i.txt" "w"];
		for {set n 1} {$n <= $TotalNodes} {incr n 1} {
			puts $ModeFile "node $n [nodeEigenvector $n $i]";
		}
		close $ModeFile;
	}
}