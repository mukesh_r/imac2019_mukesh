#=================================================
# GRAVITY ANALYSIS
#=================================================

integrator LoadControl [expr 1./$NstepG];
system BandGeneral;
test NormDispIncr 1.0e-8 100;
numberer Plain;
constraints Plain;
algorithm Newton;
analysis Static;

set ok [analyze $NstepG];

# if {$ok != 0}  {
	# puts "NOT CONVERGED for Gravity.";
	# puts "============================";
# } else {
	# puts "GRAVITY ANALYSIS DONE!!";
	# puts "=========================";
# }
loadConst -time 0.0;
set GravityAnalysis Done;