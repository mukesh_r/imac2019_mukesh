#set dataDir "Results";					# set up name of data directory
file mkdir $dataDir; 					# create data directory

#set AccNodeRec {91 85 70 64 63 57 56};
#set AccNodeRec {91 63};
set AccNodeRec {91 85 70 64 63 57 56};
set DispNodeRec {91 85 70 64 63 57 56};

# Accelerometers
set count 0;
for {set i 1} {$i<=[llength $AccNodeRec]} {incr i} {
	set count [expr $count+1];
	recorder Node -file $dataDir/($SPnum)Acc$count.txt 	-time -node [lindex $AccNodeRec [expr $i-1]] -dof 1 accel;
}

# Displacement Sensors
set count 0;
for {set i 1} {$i<=[llength $DispNodeRec]} {incr i} {
	set count [expr $count+1];
	recorder Node -file $dataDir/($SPnum)Disp$count.txt -time -node [lindex $DispNodeRec [expr $i-1]] -dof 1 disp;
}

recorder Element -ele 1 -time -file $dataDir/($SPnum)Stress.txt -time   material 1 stress;