#=================================================
# EARTHQUAKE
#=================================================

# set gAccFile [open "GM.txt" "w"];
# for {set i 1} {$i <= $NPTS} {incr i} {
	# puts $gAccFile [expr [lindex $GMDataAll [expr $i + 1]]];
# }
# close $gAccFile

# set outFile "GM.txt";

#timeSeries Path $tag -dt $dt -filePath $filePath <-factor $cFactor> <-useLast> <-prependZero> <-startTime $tStart>
#timeSeries Path $TStag -dt $dt -filePath $outFile -factor $GMfactor;

set GMpatternTag 1000;

#pattern UniformExcitation $patternTag $dir -accel $tsTag <-vel0 $vel0> <-fact $cFactor>
pattern UniformExcitation $GMpatternTag $GMdirection -accel "Series -factor 1 -filePath $GMFile -dt $DtAnalysis";

constraints Transformation;
numberer Plain;					                       
system BandGeneral;
test NormDispIncr 1.0e-8 10 0;	 
algorithm Newton;
integrator Newmark 0.50 0.25;
analysis Transient;

set startT [clock seconds]

if {$Display == "yes"} {
recorder display "Displaced shape" 30 30 600 600 -wipe;
prp 0 20 1;
vup 0 1 0;
vpn 0 0 1;
display 1 1 50;
}

source Recorders.tcl;

#set Nsteps [expr int($TmaxAnalysis/$DtAnalysis)];

set ok [analyze $Nsteps $DtAnalysis];	                                                      

set status [open "status($SPnum).txt" "w"];
if {$ok == 0} {
	puts $status "0"
} else {
	puts $status "1"
}
close $status;


set endT [clock seconds]

# puts "Earthquake action analysis completed, completed time: [expr $endT-$startT] seconds."
# puts "=========================";